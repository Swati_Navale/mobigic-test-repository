import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobigictest_app/basewidget/color.dart';
import 'package:mobigictest_app/home_screen.dart';

import 'basewidget/getButton.dart';

class GridScreen extends StatefulWidget {
  int row;
  int column;
  String text;
  GridScreen(this.row, this.column, this.text);

  @override
  _GridScreenState createState() => _GridScreenState();
}

class _GridScreenState extends State<GridScreen> {
  TextEditingController _controller = TextEditingController();
  late List<List<String>> matrix;
  var secondLastIndex,lastIndex;
  var indexList = [];
  @override
  void initState() {
    super.initState();
    createMatrix();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 4,
        title: Text("Grid Page",style: TextStyle(color: Colors.white,fontSize: 17),),
        actions: [
          Container(
            width: 100,
           // color: Colors.amber,
            child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (builder)=>HomeScreen()));
                },
                child: getButton(context, 'Reset',Colors.amber)
            ),
          )
        ],
      ),
      body: _build2DGrid(),
    );
  }

  //create matrix for search algorithm
  createMatrix(){
    matrix = List.generate(widget.row, (index) => List.generate(widget.column, (index) => '', growable: false), growable: false);
    int k = 0;
    for(int i=0; i<widget.row; i++){
      for(int j=0; j<widget.column; j++){
        matrix[i][j] = widget.text.length > k ? widget.text[k++] : '';
      }
    }
    print('matrix: ' + matrix.toString());
  }

  //build 2d array for text
  Widget _build2DGrid() {
    return ListView(
        children: [
        Container(
        margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        height: 50,
        decoration: BoxDecoration(
            color: cardColor, borderRadius: BorderRadius.circular(8.0)),
        child: TextFormField(
          controller: _controller,
          onFieldSubmitted: (query) {
          },
          onChanged: (query) {
            searchText();
          },
          textInputAction: TextInputAction.search,
          maxLines: 1,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: 'Search Grid Value',
            hintStyle: GoogleFonts.poppins(),
            border: InputBorder.none,
            prefixIcon: IconButton(
              onPressed: (){
                searchText();
              },
              icon: Icon(
                Icons.search,
                color: Colors.black87,
                size: 25,
              ),
            ),
            suffixIcon: _controller.text.isNotEmpty?IconButton(
              onPressed: (){setState(() {
                indexList.clear();
                _controller.text = '';
              });},
              icon: Icon(
                Icons.close,
                color: Colors.black87,
                size: 25,
              ),
            ):null,
          ),
        ),
      ),
      Container(
            padding: const EdgeInsets.all(8.0),
            margin: const EdgeInsets.all(8.0),
            decoration: BoxDecoration(
               //border: Border.all(color: Colors.black, width: 2.0)
            ),
            child: GridView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: widget.column,
              ),
              itemBuilder: _buildGridItems,
              itemCount: widget.row * widget.column,
            ),
          ),

     ]);
  }

  // build array elements
  Widget _buildGridItems(BuildContext context, int index) {
    return Card(
      color: highlightGrid(index),
      child: Center(
        child: index < widget.text.length?Text(widget.text[index],style: GoogleFonts.poppins(fontSize: 17),):Text(''),
      ),
    );
  }

  // used for highlight grid
  Color highlightGrid(int index){
    if(index >= widget.text.length){
      return cardColor;
    }
    if(indexList.contains(index)){
      return Colors.amberAccent;
    }
    return cardColor;
  }


  searchText(){
    bool leftToRight = checkLeftToRight();
    bool topToBottom;
    if(!leftToRight) {
      topToBottom = checkTopToBottom();
      if(!topToBottom) {
       checkDiagonal();
      }
    }
    setState(() {
    });
  }

  // left to right search(east)
  bool checkLeftToRight(){
    if(_controller.text.isEmpty){
      indexList.clear();
      return false;
    }
    if(matrix.isEmpty){
      return false;
    }
    if(widget.text.isEmpty){
      return false;
    }
    indexList.clear();
    int k=0;
    int firstIndex = widget.text.indexOf(_controller.text[0]);
    int row = firstIndex~/widget.column;
    int col = firstIndex % widget.column;
    for(; col<widget.column; col++){
      if(_controller.text.length >k && matrix[row][col] == _controller.text[k]){
        k++;
        indexList.add((row*widget.column)+col);
      }
    }

    if(indexList.length == _controller.text.length){
      for(int l=0; l<indexList.length-1; l++){
        if(indexList[l]!=indexList[l+1]-1){
          indexList.clear();
          return false;
        }
      }
      print('String matched');
      return true;
    }
    indexList.clear();
    return false;
  }

  // top to bottom search(south)
  bool checkTopToBottom(){
    if(_controller.text.isEmpty){
      return false;
    }
    if(matrix.isEmpty){
      return false;
    }
    if(widget.text.isEmpty){
      return false;
    }
    indexList.clear();
    int k=0;
    int firstIndex = widget.text.indexOf(_controller.text[0]);
    int row = firstIndex~/widget.column;
    int col = firstIndex % widget.column;
    for(; row<widget.row; row++){
      if(_controller.text.length >k && matrix[row][col] == _controller.text[k]){
        k++;
        indexList.add((row*widget.column)+col);
      }
    }
    if(indexList.length == _controller.text.length){
      for(int l=0; l<indexList.length-1; l++){
        if(indexList[l]!=indexList[l+1]-widget.column){
          indexList.clear();
          return false;
        }
      }
      print('String matched');
      return true;
    }
    indexList.clear();
    return false;
  }

  // for diagonal search(south-east)
  bool checkDiagonal(){
    if(_controller.text.isEmpty){
      return false;
    }
    if(matrix.isEmpty){
      return false;
    }
    if(widget.text.isEmpty){
      return false;
    }
    indexList.clear();
    int k=0;
    int firstIndex = widget.text.indexOf(_controller.text[0]);
    int row = firstIndex~/widget.column;
    int col = firstIndex % widget.column;
    for(; col<widget.column && row<widget.row; col++, row++){
      if(_controller.text.length >k && matrix[row][col] == _controller.text[k]){
        k++;
        indexList.add((row*widget.column)+col);
      }
    }
    if(indexList.length == _controller.text.length){
      for(int l=0; l<indexList.length-1; l++){
        if(indexList[l]!=indexList[l+1]-(widget.column + 1)){
          indexList.clear();
          return false;
        }
      }
      print('String matched');
      return true;
    }
    indexList.clear();
    return false;
  }
}
