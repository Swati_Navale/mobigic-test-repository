import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobigictest_app/basewidget/search_widget.dart';

import 'color.dart';

Widget getButton(BuildContext context,String buttonName, [MaterialColor? buttonColor]){
  return Padding(
    padding: const EdgeInsets.all(10.0),
    child: Container(
      height: 50,
      width:MediaQuery.of(context).size.width/2,
      decoration: BoxDecoration(
       color: buttonColor != null? buttonColor : Colors.blue,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Center(child: Text(buttonName,style: GoogleFonts.poppins(color: Colors.white,fontSize: 17),)),
    ),
  );
}