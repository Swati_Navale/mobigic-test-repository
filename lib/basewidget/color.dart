import 'package:flutter/material.dart';

const black = Color(0xF7050505);
const cardColor = Color(0xB37EDBFA);
const textFieldColor = Color(0xffececec);
const primaryColor = Color(0xffb12b2a);
const appbarColor = Color(0xFF263238);
const primaryDarkColor = Color(0xFF263238);
const iconColor = Color(0xFFA3ADD0);
const oddRowColor = Color(0xFFB7DEEF);