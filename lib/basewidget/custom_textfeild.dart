import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';


class CustomTextField extends StatefulWidget {
  final TextEditingController controller;
  final TextInputType textInputType;
  final int? maxLength;
  final bool isValidator;
  final String validatorMsg;
  final String labelText;
  final List<TextInputFormatter>? inputFormatters;

  CustomTextField({
    required this.controller,
    required this.labelText,
    required this.textInputType,
    this.maxLength,
    required this.validatorMsg,
    this.isValidator = false,
    this.inputFormatters,
    });

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 17,vertical: 10),
      height: widget.maxLength != null ? 70 : 50,
      child: TextFormField(
        controller: widget.controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return widget.validatorMsg;
          }
          return widget.validatorMsg;
        },
        maxLength: widget.maxLength,
        inputFormatters: widget.inputFormatters,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: widget.labelText,
          labelStyle: GoogleFonts.poppins(fontSize: 15),
        ),
        keyboardType: widget.textInputType != null ? widget.textInputType : TextInputType.text,
      ),
    );
  }
}

