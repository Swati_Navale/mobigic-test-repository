import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobigictest_app/provider/search_provider.dart';
import 'package:provider/provider.dart';

import 'color.dart';

class SearchWidget extends StatelessWidget {
  final String hintText;
  final Function? onTextChanged;
  final Function onClearPressed;
  final Function? onSubmit;
  final String? text;
  SearchWidget({required this.hintText, this.onTextChanged, required this.onClearPressed, this.onSubmit,this.text});

  @override
  Widget build(BuildContext context) {
    final TextEditingController _controller = TextEditingController();//text: Provider.of<SearchProvider>(context).searchText);
    return Container(
      margin: EdgeInsets.symmetric(vertical:20,horizontal: 20),
      height: 50,
      decoration: BoxDecoration(color: cardColor, borderRadius: BorderRadius.circular(8.0)),
      alignment: Alignment.center,
      child:TextFormField(
          controller: _controller,
          onFieldSubmitted: (query) {
            onSubmit!(query);

          },
          onChanged: (query) {
            onTextChanged!(query);
          },
          textInputAction: TextInputAction.search,
          maxLines: 1,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: hintText,
            isDense: true,
            hintStyle: GoogleFonts.poppins(),
            border: InputBorder.none,
            prefixIcon: Icon(Icons.search, color: Colors.black87,size: 25,),
           /* suffixIcon: Provider.of<SearchProvider>(context).searchText.isEmpty ? IconButton(
              icon: Icon(Icons.clear, color: Colors.black87),
              onPressed: () {
                onClearPressed();
                _controller.clear();
              },
            ) : _controller.text.isNotEmpty ? IconButton(
              icon: Icon(Icons.clear, color: Colors.black87),
              onPressed: () {
                onClearPressed();
                _controller.clear();
              },
            ) : null,*/
          ),
        ),

    );
  }
}
