
import 'package:get_it/get_it.dart';
import 'package:mobigictest_app/provider/search_provider.dart';

GetIt locator = GetIt.asNewInstance();

void setupLocator(){

  locator.registerLazySingleton(() => SearchProvider());

}