import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobigictest_app/basewidget/getButton.dart';
import 'grid_screen.dart';
import 'package:overlay_support/overlay_support.dart';

import 'basewidget/custom_textfeild.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>{

  TextEditingController rowController = new TextEditingController();
  TextEditingController columnController = new TextEditingController();
  TextEditingController textController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 4,
        title: Text("Test App",style: TextStyle(color: Colors.white,fontSize: 17),),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20,horizontal: 10),
        child: ListView(
          children: [
            CustomTextField(
              controller: rowController,
              textInputType: TextInputType.numberWithOptions(signed: false,decimal: false),
              labelText: 'Enter Number of Rows',
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              maxLength: 1,
              validatorMsg: 'Please enter number of rows',
            ),
            CustomTextField(
              controller: columnController,
              textInputType: TextInputType.number,
              labelText: 'Enter Number of Columns',
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              maxLength: 1,
              validatorMsg: 'Please enter number of columns',
            ),
            CustomTextField(
              controller: textController,
              textInputType: TextInputType.text,
              labelText: 'Enter Text',
              validatorMsg: 'Please enter some text',
            ),
            InkWell(
              onTap: (){
                int m = int.parse(rowController.text);
                int n = int.parse(columnController.text);
                if (rowController.text.toString().isEmpty || columnController.text.isEmpty) {
                  showSimpleNotification(
                      Text("Please enter number of rows and column...",style: GoogleFonts.poppins(color: Colors.white),),
                      background: Colors.red);
                  return;
                }else{
                if(m==0 || n==0){
                  showSimpleNotification(
                      Text("Please enter number of rows and column is greater than zero...",style: GoogleFonts.poppins(color: Colors.white),),
                      background: Colors.red);
                  return;
                }else {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (builder) =>
                          GridScreen(m, n, textController.text)));
                }
              }},
              child: getButton(context, 'Next')
            )
          ],
        ),
      ),
    );
  }
}
